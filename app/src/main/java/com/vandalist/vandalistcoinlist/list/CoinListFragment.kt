package com.vandalist.vandalistcoinlist.list

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.vandalist.vandalistcoinlist.R
import com.vandalist.vandalistcoinlist.data.model.response.CoinResponse
import com.vandalist.vandalistcoinlist.databinding.CoinListFragmentBinding
import kotlinx.coroutines.launch

class CoinListFragment : Fragment(R.layout.coin_list_fragment) {
    private var _binding: CoinListFragmentBinding? = null
    private val binding: CoinListFragmentBinding
        get() = _binding!!
    private val viewModel: CoinListViewModel by viewModels(factoryProducer = {CoinListViewModel.factory})
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = CoinListFragmentBinding.bind(view)
        observeUiState()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun observeUiState() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.uiState.collect {uiState->
                    when(uiState){
                        CoinListUiState.Error -> {
                            binding.loadingView.visibility=View.GONE
                            binding.errorView.visibility=View.VISIBLE
                        }
                        CoinListUiState.Loading -> {
                            binding.loadingView.visibility=View.VISIBLE
                            binding.errorView.visibility=View.GONE
                        }
                        is CoinListUiState.Success -> {
                            setUpRecyclerView(uiState.coins)

                        }
                    }
                }
            }
        }
    }
    fun setUpRecyclerView(coins:List<CoinResponse>){
        binding.loadingView.visibility=View.GONE
        binding.errorView.visibility=View.GONE
        binding.coinListRv.adapter=CoinAdapter(coins){
            val action=CoinListFragmentDirections.actionCoinListFragmentToCoinDetailsFragment(it)
                findNavController().navigate(action)
        }
    }
}