package com.vandalist.vandalistcoinlist.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vandalist.vandalistcoinlist.R
import com.vandalist.vandalistcoinlist.data.model.response.CoinResponse
import com.vandalist.vandalistcoinlist.databinding.CoinItemBinding

class CoinAdapter(var coins: List<CoinResponse>?, private val onCoinClick: (String) -> Unit) :
    RecyclerView.Adapter<CoinAdapter.CoinViewHolder>() {

    inner class CoinViewHolder(val itemBinding: CoinItemBinding,private val onCoinClick: (String) -> Unit) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(coin: CoinResponse) {
            itemBinding.coinRank.text = coin.rank.toString()
            itemBinding.itemName.text =
                itemBinding.root.context.getString(R.string.coinName, coin.name, coin.symbol)
            itemBinding.activeOrNot.setImageResource(if (coin.isActive) R.drawable.active else R.drawable.not_active)
            itemBinding.root.setOnClickListener {
                onCoinClick(coin.id)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoinViewHolder {
        val coinItemBinding =
            CoinItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CoinViewHolder(coinItemBinding,onCoinClick)
    }

    override fun getItemCount(): Int = coins!!.size

    override fun onBindViewHolder(holder: CoinViewHolder, position: Int) {
        holder.bind(coins!![position])
    }
}