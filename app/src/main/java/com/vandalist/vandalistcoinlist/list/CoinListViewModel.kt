package com.vandalist.vandalistcoinlist.list

import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.vandalist.vandalistcoinlist.App
import com.vandalist.vandalistcoinlist.data.CoinRepository
import com.vandalist.vandalistcoinlist.data.model.response.CoinResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CoinListViewModel(private val coinRepository: CoinRepository): ViewModel() {
private val _uiState= MutableStateFlow<CoinListUiState>(CoinListUiState.Loading)
    val uiState=_uiState.asStateFlow()
    companion object {
        val factory:ViewModelProvider.Factory= viewModelFactory {
            initializer {
                val application=this[ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY] as App
                CoinListViewModel(application.appContainer.coinRepository)
            }
        }
    }    init {
        fetchCoins()
    }
    private fun fetchCoins(){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {
                    _uiState.value = CoinListUiState.Success(coinRepository.getCoins())
                }catch (error:Exception){
                    _uiState.value=CoinListUiState.Error
                    Log.i("CoinListViewModel", "fetchCoins: $error")
                }
            }
        }
    }

}
sealed interface CoinListUiState{
    data object Loading:CoinListUiState
    data class Success(val coins:List<CoinResponse>):CoinListUiState
    data object Error:CoinListUiState
}