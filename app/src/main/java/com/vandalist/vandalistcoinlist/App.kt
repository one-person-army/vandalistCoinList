package com.vandalist.vandalistcoinlist

import android.app.Application
import com.vandalist.vandalistcoinlist.data.AppContainer
import com.vandalist.vandalistcoinlist.data.DefaultAppContainer

class App:Application() {
    lateinit var appContainer: AppContainer
    override fun onCreate() {
        super.onCreate()
        appContainer=DefaultAppContainer()
    }
}