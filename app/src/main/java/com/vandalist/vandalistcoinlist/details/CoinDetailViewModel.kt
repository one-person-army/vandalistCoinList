package com.vandalist.vandalistcoinlist.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.vandalist.vandalistcoinlist.App
import com.vandalist.vandalistcoinlist.data.CoinRepository
import com.vandalist.vandalistcoinlist.data.model.response.CoinDetailResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CoinDetailViewModel(private val coinRepository: CoinRepository): ViewModel() {
    private val _uiState= MutableStateFlow<CoinDetailsUiState>(CoinDetailsUiState.Loading)
     val uiState= _uiState.asStateFlow()
companion object{
    val factory:ViewModelProvider.Factory= viewModelFactory {
        initializer {
            val application=this[ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY] as App
            CoinDetailViewModel(application.appContainer.coinRepository)
        }
    }
}
    sealed interface CoinDetailsUiState{
        data object Loading:CoinDetailsUiState
        data object Error:CoinDetailsUiState
        data class Success(val response:CoinDetailResponse):CoinDetailsUiState
    }
    fun fetchCoinDetails(coinId:String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                try {

                    _uiState.value=CoinDetailsUiState.Success(coinRepository.getCoinDetails(coinId))
                }catch (error:Exception){
                    _uiState.value=CoinDetailsUiState.Error
                }
            }
        }
    }
}