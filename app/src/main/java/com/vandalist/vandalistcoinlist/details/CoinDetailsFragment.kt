package com.vandalist.vandalistcoinlist.details

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.navArgs
import com.vandalist.vandalistcoinlist.R
import com.vandalist.vandalistcoinlist.data.model.response.CoinDetailResponse
import com.vandalist.vandalistcoinlist.databinding.CoinDetailsFragmentBinding
import com.vandalist.vandalistcoinlist.databinding.CoinListFragmentBinding
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class CoinDetailsFragment : Fragment(R.layout.coin_details_fragment) {
    private var _binding: CoinDetailsFragmentBinding? = null
    private val binding: CoinDetailsFragmentBinding
        get() = _binding!!
    private val viewModel: CoinDetailViewModel by viewModels(factoryProducer = {CoinDetailViewModel.factory})
    private val args: CoinDetailsFragmentArgs by navArgs()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = CoinDetailsFragmentBinding.inflate(inflater,container,false)
        observeOnUiState()
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onStart() {
        super.onStart()
        viewModel.fetchCoinDetails(args.coinId)
    }

    fun observeOnUiState() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(state = Lifecycle.State.STARTED) {
                viewModel.uiState.collect {
                    when (it) {
                        CoinDetailViewModel.CoinDetailsUiState.Error -> {
                            binding.detailsError.visibility = View.VISIBLE
                            binding.detailsProgressBar.visibility = View.GONE
                        }

                        CoinDetailViewModel.CoinDetailsUiState.Loading -> {
                            binding.detailsProgressBar.visibility = View.VISIBLE
                            binding.detailsError.visibility = View.GONE
                        }

                        is CoinDetailViewModel.CoinDetailsUiState.Success -> {
                            binding.coinImage.setImageURI(it.response.logo)
                            binding.coinName.text = getString(R.string.coin_name,it.response.name)
                            binding.coinSymbol.text = getString(R.string.coin_symbol,it.response.symbol)
                            binding.activity.setBackgroundResource(if (it.response.isActive) R.drawable.active else R.drawable.not_active)
                            binding.coinDescription.text = it.response.description
                            binding.coinTeamRv.adapter = TeamAdapter(it.response.team)
                            binding.detailsProgressBar.visibility=View.GONE
                            binding.detailsError.visibility=View.GONE
                        }
                    }
                }
            }
        }
    }
}

