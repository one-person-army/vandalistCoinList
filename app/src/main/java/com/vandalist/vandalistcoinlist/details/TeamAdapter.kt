package com.vandalist.vandalistcoinlist.details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vandalist.vandalistcoinlist.data.model.response.Team
import com.vandalist.vandalistcoinlist.databinding.TeamMemberItemBinding

class TeamAdapter(private val members: List<Team>) :
    RecyclerView.Adapter<TeamAdapter.TeamViewHolder>() {
    inner class TeamViewHolder(private val binding: TeamMemberItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(member: Team) {
            binding.memberName.text=member.name
            binding.memberRole.text=member.position
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamViewHolder {
        val teamBinding =
            TeamMemberItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TeamViewHolder(teamBinding)
    }

    override fun getItemCount(): Int = members.size


    override fun onBindViewHolder(holder: TeamViewHolder, position: Int) {
        holder.bind(members[position])
    }
}