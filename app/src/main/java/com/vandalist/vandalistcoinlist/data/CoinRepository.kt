package com.vandalist.vandalistcoinlist.data

import com.vandalist.vandalistcoinlist.data.model.response.CoinDetailResponse
import com.vandalist.vandalistcoinlist.data.model.response.CoinResponse
import com.vandalist.vandalistcoinlist.data.network.ApiService

interface CoinRepository {
    suspend fun getCoins():List<CoinResponse>
    suspend fun getCoinDetails(coinId:String):CoinDetailResponse
}
class CoinRepositoryImpl(private val apiService: ApiService):CoinRepository{
    override suspend fun getCoins(): List<CoinResponse> =apiService.getCoins()
    override suspend fun getCoinDetails(coinId: String): CoinDetailResponse =apiService.getCoinDetail(coinId)

}