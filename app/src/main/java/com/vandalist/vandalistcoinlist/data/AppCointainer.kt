package com.vandalist.vandalistcoinlist.data

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

interface AppContainer {
    val coinRepository:CoinRepository
}
class DefaultAppContainer:AppContainer{
    private val retrofit= Retrofit.Builder()
        .baseUrl("https://api.coinpaprika.com/v1/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    override val coinRepository: CoinRepository by lazy { CoinRepositoryImpl(retrofit.create())
    }

}