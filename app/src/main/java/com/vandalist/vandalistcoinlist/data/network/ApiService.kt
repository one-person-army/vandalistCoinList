package com.vandalist.vandalistcoinlist.data.network

import com.vandalist.vandalistcoinlist.data.model.response.CoinDetailResponse
import com.vandalist.vandalistcoinlist.data.model.response.CoinResponse
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {
    @GET("coins")
    suspend fun getCoins():List<CoinResponse>
    @GET("coins/{id}")
    suspend fun getCoinDetail(@Path("id")id:String):CoinDetailResponse
}