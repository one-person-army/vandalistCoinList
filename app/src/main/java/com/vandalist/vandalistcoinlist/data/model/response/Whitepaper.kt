package com.vandalist.vandalistcoinlist.data.model.response


import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Whitepaper(
    val link: String,
    val thumbnail: String
) : Parcelable