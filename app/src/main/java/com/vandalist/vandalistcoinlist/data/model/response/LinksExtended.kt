package com.vandalist.vandalistcoinlist.data.model.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class LinksExtended(
    val stats: Stats,
    val type: String,
    val url: String
) : Parcelable